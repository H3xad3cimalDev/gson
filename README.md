# GSON
Simple JSON Parser for GreyScript.

## Install
You goto the `lib.lua` file and paste it into any file on your GreyHack PC.

then you can use `import_code` to include it and it will load.

## Usage
the basic functions are.

```javascript
JSON.Parse(string text)                         // Converts JSON text into a map or whatever the JSON represents
JSON.Stringify(map | list data, beautify=false) // Converts a map or a list into valid JSON, beautify if turned to true by which default is false will generate beutified code
JSON.Beautify(string text)                      // Turns JSON text into easier to look JSON
```

### Parsing and Evaluating
If you would like you can manually parse and evaluate your JSON

you would probably want to do this so you can check if the JSON data is corrupt / incorrect.

Here's an example function

```lua
// Attempts to parse the JSON if it finds an error it will print the errors and return null
TryParse = function(text)
    parser = new JSON.Parser
    parser.Init(text)

    parserResult = parser.Parse()

    if parser.Diagnostics.GetDiagnostics().len > 0 then
        for message in parser.Diagnostics.GetDiagnosticMessages()
            print("<color=#ff0000>ERROR: " + message + "</color>")
        end for

        return null
    end if

    evaluator = new JSON.Evaluator
    evaluator.Init(parserResult)
    return evaluator.Evaluate()
end function
```

### Diagnostic Errors
The Parser and Lexer both have a `JSON.DiagnosticBag` which is a wrapper to hold `JSON.Diagnostic` instances.

the property for both the Parser and Lexer is called `Diagnostics`

### Diagnostic Bag
Diagnostic Bags only really have two useful functions for you. `GetDiagnostics` and `GetDiagnosticMessages`

`GetDiagnostics` Returns every `JSON.Diagnostic` instance the bag holds.

`GetDiagnosticMessages` Returns every message in the diagnostics.

### Diagnostic
`JSON.Diagnostic` holds 3 properties `Message`, `Span`, `Code`

`Message` is the text message, you should probably output it.

`Span` is a `JSON.TextSpan` instance.

`Code` Error code, it's a int.

### Text Span
`JSON.TextSpan` holds 3 properties `Start`, `End`, `Length`

`Start` is the start position of whatever the TextSpan is pointing to

`Length` is the length of the span

`End` is the ending position

### Error Codes
Currently there are only a few possible errors. Each error code is contained in `JSON.Diagnostic.Codes`

Here's a list of all codes
```
UNKOWN_TOKEN   
UNKOWN_IDENTIFIER
INVALID_NUMBER
DOUBLE_DOT_FLOAT 
UNTERMINATED_STRING
EXPECTED_TOKEN_KIND
EXPECTED_VALUE_TOKEN
```

All the codes are self explanatory in the names.

## How to modify?
Get [Synapse](https://gitlab.com/H3xad3cimalDev/synapse) then paste the `GSON.sef` file onto your

GreyHack PC, and type
```console
$ synapse manifest GSON.sef
```

and it should load the project.