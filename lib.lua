JSON = {}
JSON.Kind = {}
JSON.Kind.NumberToken = 0
JSON.Kind.StringToken = 1
JSON.Kind.BooleanToken = 2
JSON.Kind.NullToken = 3
JSON.Kind.WhitespaceToken = 4
JSON.Kind.OpenBracketToken = 5
JSON.Kind.CloseBracketToken = 6
JSON.Kind.OpenBraceToken = 7
JSON.Kind.CloseBraceToken = 8
JSON.Kind.ColonToken = 9
JSON.Kind.CommaToken = 10
JSON.Kind.EOLToken = 11
JSON.Kind.EOFToken = 12
JSON.Kind.UnkownToken = 13
JSON.Kind.UnkownIdentifierToken = 14
JSON.Kind.TabToken = 15
JSON.Kind.KeypairExpression = 16
JSON.Kind.ValueExpression = 17
JSON.Kind.ObjectExpression = 18
JSON.Kind.ArrayExpression = 19
JSON.Kind.ToString = function(kind)
for pk in JSON.Kind
if pk.value == kind then
return pk.key
end if
end for
end function
JSON.TextSpan = {"Start":-1, "End":-1, "Length":-1}
JSON.TextSpan.Init = function(start, length)
self.Start = start
self.Length = length
self.End = start + length
end function
JSON.TextSpan.Init2End = function(start, endn)
self.Start = start
self.End = endn
self.Length = endn - start
end function
JSON.Token = {"Kind":-1, "Position":-1, "Text":"", "Value":"", "Span":[]}
JSON.Token.Init = function(Kind, Position, Text, Value)
self.Kind = Kind
self.Position = Position
self.Text = Text
self.Value = Value
self.Span = new JSON.TextSpan
if Text == null then
self.Span.Init(Position, 0)
else
self.Span.Init(Position, Text.len)
end if
end function
JSON.Diagnostic = {"Message":"", "Span":{}, "Code":-1}
JSON.Diagnostic.Codes = {}
JSON.Diagnostic.Codes.UNKOWN_TOKEN        = 0
JSON.Diagnostic.Codes.UNKOWN_IDENTIFIER   = 1
JSON.Diagnostic.Codes.INVALID_NUMBER      = 2
JSON.Diagnostic.Codes.DOUBLE_DOT_FLOAT    = 3
JSON.Diagnostic.Codes.UNTERMINATED_STRING = 4
JSON.Diagnostic.Codes.EXPECTED_TOKEN_KIND  = 5
JSON.Diagnostic.Codes.EXPECTED_VALUE_TOKEN = 6
JSON.Diagnostic.Init = function(message, span, code)
self.Message = message
self.Span = span
self.Code = code
end function
JSON.Diagnostic.UnkownToken = function(token)
diagnostic = new JSON.Diagnostic
diagnostic.Init("Unknown token '" + token.Text + "'", token.Span, JSON.Diagnostic.Codes.UNKOWN_TOKEN)
return diagnostic
end function
JSON.Diagnostic.UnknownIdentifier = function(token)
diagnostic = new JSON.Diagnostic
diagnostic.Init("Unknown identifier '" + token.Text + "'", token.Span, JSON.Diagnostic.Codes.UNKOWN_IDENTIFIER)
return diagnostic
end function
JSON.Diagnostic.InvalidNumber = function(start, text)
diagnostic = new JSON.Diagnostic
span = new JSON.TextSpan
span.Init(start, text.len)
diagnostic.Init("'" + text + "' is not a valid number", span, JSON.Diagnostic.Codes.INVALID_NUMBER)
return diagnostic
end function
JSON.Diagnostic.DoubleDotFloat = function(start, text)
diagnostic = new JSON.Diagnostic
span = new JSON.TextSpan
span.Init(start, text.len)
diagnostic.Init("'" + text + "' is not a valid number", span, JSON.Diagnostic.Codes.DOUBLE_DOT_FLOAT)
return diagnostic
end function
JSON.Diagnostic.ExpectedTokenKind = function(kind, token)
diagnostic = new JSON.Diagnostic
diagnostic.Init("Expected <" + JSON.Kind.ToString(kind) + ">, got <" + JSON.Kind.ToString(token.Kind) + ">", token.Span, JSON.Diagnostic.Codes.EXPECTED_TOKEN_KIND)
return diagnostic
end function
JSON.Diagnostic.ExpectValueToken = function(token)
diagnostic = new JSON.Diagnostic
diagnostic.Init("Expected a value token, got <" + JSON.Kind.ToString(token.Kind) + ">", token.Span, JSON.Diagnostic.Codes.EXPECTED_VALUE_TOKEN)
return diagnostic
end function
JSON.Diagnostic.UnterminatedString = function(start, text, atext)
diagnostic = new JSON.Diagnostic
span = new JSON.TextSpan
span.Init(start, text.len)
diagnostic.Init("Unterminated string literal (""" + atext + """)", span, JSON.Diagnostic.Codes.UNTERMINATED_STRING)
return diagnostic
end function
JSON.DiagnosticBag = {"__raw__": []}
JSON.DiagnosticBag.Init = function()
self.__raw__ = []
end function
JSON.DiagnosticBag.AddDiagnostic = function(diagnostic)
self.__raw__.push(diagnostic)
end function
JSON.DiagnosticBag.GetDiagnosticMessages = function()
messages = []
for diagnostic in self.__raw__
messages.push(diagnostic.Message)
end for
return messages
end function
JSON.DiagnosticBag.GetDiagnostics = function()
return self.__raw__
end function
JSON.DiagnosticBag.Concat = function(diagnosticBag)
self.__raw__ = self.__raw__ + diagnosticBag.__raw__
end function
JSON.Lexer = {"_text":"", "_position":0, "Diagnostics":[]}
JSON.Lexer.Init = function(text)
self._text = text
self._position = 0
self.Diagnostics = new JSON.DiagnosticBag
self.Diagnostics.Init()
end function
JSON.Lexer.Peek = function(offset)
index = self._position + offset
if index >= self._text.len then
return char(0)
end if
return self._text[index]
end function
JSON.Lexer.Lookahead = function()
return self.Peek(1)
end function
JSON.Lexer.GetCurrent = function()
return self.Peek(0)
end function
JSON.Lexer.Next = function()
self._position = self._position + 1
end function
JSON.Lexer.CreateToken = function(Kind, Position, Text, Value)
token = new JSON.Token
token.Init(Kind, Position, Text, Value)
return token
end function
JSON.Lexer.IsAtEnd = function()
return self._position >= self._text.len
end function
JSON.Lexer.HexToDecimal = function(x)
x = x.upper;
bits = [];
for i in range(0,x.len -1)
if i != 0 then bits.push(x[i]);
end for
for i in range(0,bits.len -1)
if typeof(bits[i].to_int) != "number" then
if bits[i].upper == "A" then bits[i] = 10 * (16 ^ i);
if bits[i].upper == "B" then bits[i] = 11 * (16 ^ i);
if bits[i].upper == "C" then bits[i] = 12 * (16 ^ i);
if bits[i].upper == "D" then bits[i] = 13 * (16 ^ i);
if bits[i].upper == "E" then bits[i] = 14 * (16 ^ i);
if bits[i].upper == "F" then bits[i] = 15 * (16 ^ i);
else
bits[i] = bits[i].to_int * (16 ^ i);
end if
end for
decimal = 0;
for i in range(0,bits.len -1)
decimal = decimal + bits[i];
end for
return str(decimal);
end function
JSON.Lexer.Lex = function()
if self.IsAtEnd() then
return self.CreateToken(JSON.Kind.EOFToken, self._position, char(0), null)
end if
if typeof(self.GetCurrent().to_int) == "number" then
start = self._position
isFloat = false
brokeDoubleFloatRule = false
while true
if self.GetCurrent() == "." then
if not isFloat then
isFloat = true
self.Next()
continue
else
brokeDoubleFloatRule = true
self.Next()
continue
end if
end if
if not typeof(self.GetCurrent().to_int) == "number" or self.IsAtEnd() then break end if
self.Next()
end while
if brokeDoubleFloatRule then
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.DoubleDotFloat(start, self._text[start:self._position]))
end if
text = self._text[start:self._position]
value = null
if isFloat then
value = text.val
else
value = text.to_int
end if
if value == null then
if not brokeDoubleFloatRule then
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.InvalidNumber(start, text))
end if
end if
return self.CreateToken(JSON.Kind.NumberToken, start, text, value)
end if
if self.GetCurrent() == """" then
start = self._position
self.Next()
text = ""
while true
if self.IsAtEnd() or self.GetCurrent() == char(10) then
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.UnterminatedString(start, self._text[start:self._position], text))
break
end if
if self.GetCurrent() == """" then
self.Next()
break
end if
if self.GetCurrent() == "\" then
self.Next()
if self.GetCurrent() == "u" then
self.Next()
hex = self.GetCurrent()
self.Next()
hex = hex + self.GetCurrent()
self.Next()
hex = hex + self.GetCurrent()
self.Next()
hex = hex + self.GetCurrent()
character = char(self.HexToDecimal(hex))
text = text + character
self.Next()
continue
else if self.GetCurrent() == """" then
text = text + """"
self.Next()
continue
else if self.GetCurrent() == "\" then
text = text + "\"
self.Next()
continue
else if self.GetCurrent() == "/" then
text = text + "/"
self.Next()
continue
else if self.GetCurrent() == "b" then
text = text + char(8)
self.Next()
continue
else if self.GetCurrent() == "f" then
text = text + char(12)
self.Next()
continue
else if self.GetCurrent() == "n" then
text = text + char(10)
self.Next()
continue
else if self.GetCurrent() == "r" then
text = text + char(13)
self.Next()
continue
else if self.GetCurrent() == "t" then
text = text + char(9)
self.Next()
continue
end if
end if
text = text + self.GetCurrent()
self.Next()
end while
tokenText = self._text[start:self._position]
return self.CreateToken(JSON.Kind.StringToken, start, tokenText, text)
end if
if self.GetCurrent() == " " then
start = self._position
while true
if self.GetCurrent() != " " or self.IsAtEnd() then break end if
self.Next()
end while
text = self._text[start:self._position]
return self.CreateToken(JSON.Kind.WhitespaceToken, start, text, null)
end if
if self.GetCurrent() == char(9) then
self.Next()
return self.CreateToken(JSON.Kind.TabToken, self._position - 1, char(9), null)
end if
if self.GetCurrent() == "{" then
self.Next()
return self.CreateToken(JSON.Kind.OpenBraceToken, self._position - 1, "{", null)
end if
if self.GetCurrent() == "}" then
self.Next()
return self.CreateToken(JSON.Kind.CloseBraceToken, self._position - 1, "}", null)
end if
if self.GetCurrent() == "[" then
self.Next()
return self.CreateToken(JSON.Kind.OpenBracketToken, self._position - 1, "[", null)
end if
if self.GetCurrent() == "]" then
self.Next()
return self.CreateToken(JSON.Kind.CloseBracketToken, self._position - 1, "]", null)
end if
if self.GetCurrent() == ":" then
self.Next()
return self.CreateToken(JSON.Kind.ColonToken, self._position - 1, ":", null)
end if
if self.GetCurrent() == "," then
self.Next()
return self.CreateToken(JSON.Kind.CommaToken, self._position - 1, ",", null)
end if
if "abcdefghijklmnopqrstuvwxyz".split("").indexOf(self.GetCurrent()) != null then
start = self._position
self.Next()
while true
if "abcdefghijklmnopqrstuvwxyz".split("").indexOf(self.GetCurrent()) == null or self.IsAtEnd() then break end if
self.Next()
end while
text = self._text[start:self._position]
if text == "true" then
return self.CreateToken(JSON.Kind.BooleanToken, start, text, true)
else if text == "false" then
return self.CreateToken(JSON.Kind.BooleanToken, start, text, false)
else if text == "null" then
return self.CreateToken(JSON.Kind.NullToken, start, text, null)
else
unkownIndentifier = self.CreateToken(JSON.Kind.UnkownIdentifierToken, start, text, null)
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.UnknownIdentifier(unkownIndentifier))
return unkownIndentifier
end if
end if
if self.GetCurrent() == char(10) then
self.Next()
return self.CreateToken(JSON.Kind.EOLToken, self._position - 1, char(10), null)
end if
self.Next()
unkownToken = self.CreateToken(JSON.Kind.UnknownToken, self._position - 1, self.GetCurrent(), null)
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.UnkownToken(unkownToken))
return unkownToken
end function
JSON.KeypairExpression = {"NameToken":"", "ValueExpression":"", "SeperatorToken":"", "Kind":JSON.Kind.KeypairExpression}
JSON.KeypairExpression.Init = function(nameToken, seperatorToken, valueExpression)
self.NameToken = nameToken
self.SeperatorToken = seperatorToken
self.ValueExpression = valueExpression
self.Kind = JSON.Kind.KeypairExpression
end function
JSON.ValueExpression = {"ValueToken":"", "Kind":JSON.Kind.ValueExpression}
JSON.ValueExpression.Init = function(valueToken)
self.ValueToken = valueToken
self.Kind = JSON.Kind.ValueExpression
end function
JSON.ObjectExpression = {"OpeningBraceToken":"", "Pairs":[], "ClosingBraceToken":"", "Kind":JSON.Kind.ObjectExpression}
JSON.ObjectExpression.Init = function(openingBraceToken, pairs, closingBraceToken)
self.OpeningBraceToken = openingBraceToken
self.Pairs = pairs
self.ClosingBraceToken = closingBraceToken
self.Kind = JSON.Kind.ObjectExpression
end function
JSON.ArrayExpression = {"OpeningBracketToken":"", "Values":[], "ClosingBracketToken":"", "Kind":JSON.Kind.ArrayExpression}
JSON.ArrayExpression.Init = function(openingBracketToken, values, closingBracketToken)
self.OpeningBracketToken = openingBracketToken
self.Values = values
self.ClosingBracketToken = closingBracketToken
self.Kind = JSON.Kind.ArrayExpression
end function
JSON.Parser = {"_text":"", "_position":0, "Diagnostics":[], "tokens":[]}
JSON.Parser.Init = function(text)
self._text = text
self._position = 0
self.Diagnostics = new JSON.DiagnosticBag
self.Diagnostics.Init()
self.tokens = []
lexer = new JSON.Lexer
lexer.Init(text)
while true
token = lexer.Lex()
if token.Kind != JSON.Kind.WhitespaceToken and token.Kind != JSON.Kind.EOLToken and token.Kind != JSON.Kind.UnkownToken and token.Kind != JSON.Kind.UnkownIdentifierToken and token.Kind != JSON.Kind.TabToken then
self.tokens.push(token)
end if
if token.Kind == JSON.Kind.EOFToken then
break
end if
end while
self.Diagnostics.Concat(lexer.Diagnostics)
end function
JSON.Parser.Peek = function(offset)
index = self._position + offset
if index >= self.tokens.len then
return self.tokens[self.tokens.len - 1]
end if
return self.tokens[index]
end function
JSON.Parser.Lookahead = function()
return self.Peek(1)
end function
JSON.Parser.GetCurrent = function()
return self.Peek(0)
end function
JSON.Parser.NextToken = function()
token = self.GetCurrent()
self._position = self._position + 1
return token
end function
JSON.Parser.MatchToken = function(kind)
if self.GetCurrent().Kind == kind then
return self.NextToken()
end if
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.ExpectedTokenKind(kind, self.GetCurrent()))
token = new JSON.Token
token.Init(kind, self.GetCurrent().Position, null, null)
self.NextToken()
return token
end function
JSON.Parser.Parse = function()
if self.GetCurrent().Kind == JSON.Kind.OpenBraceToken then
return self.ParseObject()
else if self.GetCurrent().Kind == JSON.Kind.OpenBracketToken then
return self.ParseArray()
end if
end function
JSON.Parser.ParseObject = function()
openingBraceToken = self.MatchToken(JSON.Kind.OpenBraceToken)
pairs = []
closingBraceToken = null
if self.GetCurrent().Kind == JSON.Kind.CloseBraceToken then
closingBraceToken = self.NextToken()
objectExpression = new JSON.ObjectExpression
objectExpression.Init(openingBraceToken, pairs, closingBraceToken)
return objectExpression
end if
while true
if self.GetCurrent().Kind == JSON.Kind.EOFToken then
break
end if
key = self.ParseKeypair()
pairs.push(key)
if self.GetCurrent().Kind != JSON.Kind.CommaToken and self.Lookahead().Kind != JSON.Kind.CloseBraceToken then
break
end if
self.MatchToken(JSON.Kind.CommaToken)
end while
closingBraceToken = self.MatchToken(JSON.Kind.CloseBraceToken)
objectExpression = new JSON.ObjectExpression
objectExpression.Init(openingBraceToken, pairs, closingBraceToken)
return objectExpression
end function
JSON.Parser.ParseArray = function()
openingBracketToken = self.MatchToken(JSON.Kind.OpenBracketToken)
values = []
closingBracketToken = null
if self.GetCurrent().Kind == JSON.Kind.CloseBracketToken then
closingBracketToken = self.NextToken()
arrayExpression = new JSON.ArrayExpression
arrayExpression.Init(openingBracketToken, values, closingBracketToken)
return arrayExpression
end if
while true
if self.GetCurrent().Kind == JSON.Kind.EOFToken then
break
end if
value = self.ParseValue()
values.push(value)
if self.GetCurrent().Kind != JSON.Kind.CommaToken and self.Lookahead().Kind != JSON.Kind.CloseBracketToken then
break
end if
self.MatchToken(JSON.Kind.CommaToken)
end while
closingBracketToken = self.MatchToken(JSON.Kind.CloseBracketToken)
arrayExpression = new JSON.ArrayExpression
arrayExpression.Init(openingBracketToken, values, closingBracketToken)
return arrayExpression
end function
JSON.Parser.ParseValue = function()
token = self.GetCurrent()
if token.Kind == JSON.Kind.StringToken or token.Kind == JSON.Kind.NumberToken or token.Kind == JSON.Kind.BooleanToken or token.Kind == JSON.Kind.NullToken then
valueExpression = new JSON.ValueExpression
valueExpression.Init(self.NextToken())
return valueExpression
else if token.Kind == JSON.Kind.OpenBraceToken then
return self.ParseObject()
else if token.Kind == JSON.Kind.OpenBracketToken then
return self.ParseArray()
else
self.Diagnostics.AddDiagnostic(JSON.Diagnostic.ExpectValueToken(token))
valueExpression = new JSON.ValueExpression
newToken = new JSON.Token
newToken.Init(JSON.Kind.NullToken, token.Position, null, null)
valueExpression.Init(newToken)
return valueExpression
end if
end function
JSON.Parser.ParseKeypair = function()
key = self.MatchToken(JSON.Kind.StringToken)
seperator = self.MatchToken(JSON.Kind.ColonToken)
value = self.ParseValue()
keypairExpression = new JSON.KeypairExpression
keypairExpression.Init(key, seperator, value)
return keypairExpression
end function
JSON.Evaluator = {"root":""}
JSON.Evaluator.Init = function(root)
self.root = root
end function
JSON.Evaluator.Evaluate = function()
return self.EvaluateExpression(self.root)
end function
JSON.Evaluator.EvaluateExpression = function(node)
if node.Kind == JSON.Kind.KeypairExpression then
return {"key":node.NameToken.Value, "value":self.EvaluateExpression(node.ValueExpression)}
end if
if node.Kind == JSON.Kind.ValueExpression then
return node.ValueToken.Value
end if
if node.Kind == JSON.Kind.ArrayExpression then
array = []
for i in node.Values
array.push(self.EvaluateExpression(i))
end for
return array
end if
if node.Kind == JSON.Kind.ObjectExpression then
object = {}
for keyPairExpression in node.Pairs
pair = self.EvaluateExpression(keyPairExpression)
object[pair.key] = pair.value
end for
return object
end if
end function
JSON.Generator = {}
JSON.Generator.GenerateValue = function(value)
if typeof(value) == "string" then
value = value.replace("""", "\""")
value = value.replace(char(10), "\n")
value = value.replace(char(9), "\t")
value = value.replace(char(8), "\b")
value = value.replace(char(12), "\f")
value = value.replace(char(13), "\r")
value = """" + value + """"
else if typeof(value) == "number" then
value = str(value)
else if typeof(value) == "map" then
value = self.GenerateObject(value)
else if typeof(value) == "list" then
value = self.GenerateArray(value)
else
value = "null"
end if
return value
end function
JSON.Generator.GenerateKeypair = function(key, value)
return """" + key + """:" + self.GenerateValue(value)
end function
JSON.Generator.GenerateObject = function(object)
objstr = "{"
pairs = []
for k in object
pairs.push(self.GenerateKeypair(k.key, k.value))
end for
objstr = objstr + pairs.join(",") + "}"
return objstr
end function
JSON.Generator.GenerateArray = function(array)
arrstr = "["
values = []
for v in array
values.push(self.GenerateValue(v))
end for
arrstr = arrstr + values.join(",") + "]"
return arrstr
end function
JSON.Generator.Generate = function(object)
if typeof(object) == "map" then
return self.GenerateObject(object)
else if typeof(object) == "list" then
return self.GenerateArray(object)
end if
end function
JSON.Beautifier = {"_text":"", "tabbing":""}
JSON.Beautifier.Init = function(text)
self._text = text
self.tabbing = ""
end function
JSON.Beautifier.Beautify = function()
parser = new JSON.Parser
parser.Init(self._text)
return self.StringifyExpression(parser.Parse())
end function
JSON.Beautifier.StringifyValue = function(node)
if node.Kind == JSON.Kind.ValueExpression then
value = node.ValueToken.Value
if typeof(value) == "string" then
value = value.replace("""", "\""")
value = value.replace(char(10), "\n")
value = value.replace(char(9), "\t")
value = value.replace(char(8), "\b")
value = value.replace(char(12), "\f")
value = value.replace(char(13), "\r")
return """" + value + """"
else if typeof(value) == "number" then
return str(value)
else
return "null"
end if
else if node.Kind == JSON.Kind.ObjectExpression then
return self.StringifyObject(node)
else if node.Kind == JSON.Kind.ArrayExpression then
return self.StringifyArray(node)
else
return "null"
end if
end function
JSON.Beautifier.TabIndent = function()
self.tabbing = self.tabbing + char(9)
return self.tabbing
end function
JSON.Beautifier.TabUnindent = function()
self.tabbing = self.tabbing[1:]
return self.tabbing
end function
JSON.Beautifier.StringifyObject = function(expression)
if expression.Pairs.len == 0 then
return "{}"
end if
objstr = "{" + char(10)
self.TabIndent()
pairsstr = ""
for k in expression.Pairs
pairsstr = pairsstr + self.tabbing + self.StringifyKeypair(k) + "," + char(10)
end for
objstr = objstr + pairsstr[:-2] + char(10) + self.TabUnindent() + "}"
return objstr
end function
JSON.Beautifier.StringifyArray = function(expression)
if expression.Values.len == 0 then
return "[]"
end if
arrstr = "[" + char(10)
self.TabIndent()
valuesstr = ""
for v in expression.Values
valuesstr = valuesstr + self.tabbing + self.StringifyValue(v) + "," + char(10)
end for
arrstr = arrstr + valuesstr[:-2] + char(10) + self.TabUnindent() + "]"
return arrstr
end function
JSON.Beautifier.StringifyKeypair = function(expression)
key = expression.NameToken.Value
stringifiedValue = self.StringifyValue(expression.ValueExpression)
return """" + key + """: " + stringifiedValue
end function
JSON.Beautifier.StringifyExpression = function(node)
if node.Kind == JSON.Kind.ObjectExpression then
return self.StringifyObject(node)
else if node.Kind == JSON.Kind.ArrayExpression then
return self.StringifyArray(node)
end if
end function
JSON.BeautyGenerator = {"tabbing":""}
JSON.BeautyGenerator.GenerateValue = function(value)
if typeof(value) == "string" then
value = value.replace("""", "\""")
value = value.replace(char(10), "\n")
value = value.replace(char(9), "\t")
value = value.replace(char(8), "\b")
value = value.replace(char(12), "\f")
value = value.replace(char(13), "\r")
value = """" + value + """"
else if typeof(value) == "number" then
value = str(value)
else if typeof(value) == "map" then
value = self.GenerateObject(value)
else if typeof(value) == "list" then
value = self.GenerateArray(value)
else
value = "null"
end if
return value
end function
JSON.BeautyGenerator.TabIndent = function()
self.tabbing = self.tabbing + char(9)
return self.tabbing
end function
JSON.BeautyGenerator.TabUnindent = function()
self.tabbing = self.tabbing[1:]
return self.tabbing
end function
JSON.BeautyGenerator.GenerateKeypair = function(key, value)
return """" + key + """: " + self.GenerateValue(value)
end function
JSON.BeautyGenerator.GenerateObject = function(object)
objstr = "{" + char(10)
pairs = []
self.TabIndent()
for k in object
pairs.push(self.tabbing + self.GenerateKeypair(k.key, k.value) + "," + char(10))
end for
objstr = objstr + pairs.join()[:-2] + char(10) + self.TabUnindent() + "}"
return objstr
end function
JSON.BeautyGenerator.GenerateArray = function(array)
arrstr = "[" + char(10)
values = []
self.TabIndent()
for v in array
values.push(self.tabbing + self.GenerateValue(v) + "," + char(10))
end for
arrstr = arrstr + values.join()[:-2] + char(10) + self.TabUnindent() + "]"
return arrstr
end function
JSON.BeautyGenerator.Generate = function(object)
if typeof(object) == "map" then
return self.GenerateObject(object)
else if typeof(object) == "list" then
return self.GenerateArray(object)
end if
end function
JSON.Parse = function(text)
parser = new JSON.Parser
parser.Init(text)
parserResult = parser.Parse()
evaluator = new JSON.Evaluator
evaluator.Init(parserResult)
return evaluator.Evaluate()
end function
JSON.Stringify = function(object, beautify=false)
if not beautify then
generator = new JSON.Generator
return generator.Generate(object)
else
generator = new JSON.BeautyGenerator
return generator.Generate(object)
end if
end function
JSON.Beautify = function(text)
beautifier = new JSON.Beautifier
beautifier.Init(text)
return beautifier.Beautify()
end function
JSON.Minify = function(text)
minifier = new JSON.Minifier
minifier.Init(text)
return minifier.Minify()
end function